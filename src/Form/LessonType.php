<?php

namespace App\Form;

use App\Entity\Lesson;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LessonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name', 
                TextType::class, [
                'label' => 'Nom'
                ]
            )
            ->add(
                'startDate', 
                DateTimeType::class, 
                [
                    'label' => 'Date de début',
                ]
            )
            ->add(
                'endDate',
                DateTimeType::class, 
                [
                'label' => 'Date de début',
                ]
            )
            ->add('classroom');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lesson::class,
        ]);
    }
}
