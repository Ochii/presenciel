<?php

namespace App\Form;

use App\Entity\Classroom;
use App\Entity\School;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassroomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name', 
                TextType::class, [
                'label' => 'Nom'
                ]
            )
            ->add('promotion')
            ->add(
                'amStart', 
                TimeType::class, [
                'label' => 'Début Matin'
                ]
            )
            ->add(
                'amEnd', 
                TimeType::class, [
                'label' => 'Fin Matin'    
                ]
            )
            ->add(
                'pmStart',
                TimeType::class, [
                'label' => 'Début Après-midi'
                ]
            )
            ->add(
                'pmEnd', 
                TimeType::class, [
                'label' => 'Fin Après-midi'
                ]
            )
            ->add(
                'school', 
                EntityType::class, [
                'class' =>School::class,
                'label' => 'Ecole'
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Classroom::class,
        ]);
    }
}
