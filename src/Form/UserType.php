<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserType extends AbstractType
{
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('mail')
            ->add('password', PasswordType::class);
        if ($this->tokenStorage->getToken()->getUser()->getRoles()[0] != 'ROLE_ETUDIANT') {
            $builder
                ->add(
                    'status', 
                    ChoiceType::class, [
                        'choices' => [
                            'Etudiant' => 'etudiant',
                            'Encadrement' => 'encadrement',
                            'Formateur' => 'formateur',
                            'Administrateur' => 'admin'
                        ]
                    ]
                )
                ->add('classroom')
                ->add('school');
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
