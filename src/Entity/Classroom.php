<?php

namespace App\Entity;

use App\Repository\ClassroomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="classrooms")
 * @ORM\Entity(repositoryClass=ClassroomRepository::class)
 */
class Classroom
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $promotion;

    /**
     * @ORM\ManyToOne(targetEntity=School::class, inversedBy="classrooms")
     */
    private $school;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="classroom")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Lesson::class, mappedBy="classroom")
     */
    private $lessons;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $amStart;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $amEnd;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $pmStart;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $pmEnd;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->lessons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPromotion(): ?string
    {
        return $this->promotion;
    }

    public function setPromotion(string $promotion): self
    {
        $this->promotion = $promotion;

        return $this;
    }

    public function getSchool(): ?School
    {
        return $this->school;
    }

    public function setSchool(?School $school): self
    {
        $this->school = $school;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setClassroom($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getClassroom() === $this) {
                $user->setClassroom(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
            $lesson->setClassroom($this);
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
            // set the owning side to null (unless already changed)
            if ($lesson->getClassroom() === $this) {
                $lesson->setClassroom(null);
            }
        }

        return $this;
    }

    public function getAmStart(): ?\DateTimeInterface
    {
        return $this->amStart;
    }

    public function setAmStart(?\DateTimeInterface $amStart): self
    {
        $this->amStart = $amStart;

        return $this;
    }

    public function getAmEnd(): ?\DateTimeInterface
    {
        return $this->amEnd;
    }

    public function setAmEnd(?\DateTimeInterface $amEnd): self
    {
        $this->amEnd = $amEnd;

        return $this;
    }

    public function getPmStart(): ?\DateTimeInterface
    {
        return $this->pmStart;
    }

    public function setPmStart(?\DateTimeInterface $pmStart): self
    {
        $this->pmStart = $pmStart;

        return $this;
    }

    public function getPmEnd(): ?\DateTimeInterface
    {
        return $this->pmEnd;
    }

    public function setPmEnd(?\DateTimeInterface $pmEnd): self
    {
        $this->pmEnd = $pmEnd;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
