<?php

namespace App\Controller;

use App\Entity\Classroom;
use App\Entity\User;
use App\Repository\ClassroomRepository;
use App\Repository\SchoolRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/signing")
 */
class SigningController extends AbstractController
{
    public function __construct(
        ClassroomRepository $classroomRepository,
        UserRepository $userRepository,
        SchoolRepository $schoolRepository
    )
    {
        $this->classRepo = $classroomRepository;
        $this->userRepo = $userRepository;
        $this->schoolRepo = $schoolRepository;
    }

    /**
     * @Route("/", name="signing_index")
     */
    public function index(Request $request)
    {
        // Récupérer l'école de l'utilisateur log
        $school = $this->schoolRepo->findOneById($this->getUser()->getSchool());
        if ($school) {
            $school = $school;
        } else {
            $school = null;
        }

        //Récupération de l'heure pour filtrage
        date_default_timezone_set("Europe/Paris");
        $time = (int)date('H');

        //Récupération de la liste des élèves de la classe
        if ($request->get('classroom')) {
            $students = $this->userRepo->findBy(
                ['classroom' => $request->get('classroom')]
            );
        } else {
            $students = [];
        }
        return $this->render(
            'signing/index.html.twig',
            [
                'students' => $students,
                'schools' => $school,
                'time' => $time
            ]
        );

    }
}
