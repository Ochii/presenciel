<?php

namespace App\Controller;

use App\Entity\Classroom;
use App\Entity\Lesson;
use App\Entity\User;
use App\Form\LessonType;
use App\Repository\LessonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lesson")
 */
class LessonController extends AbstractController
{
    /**
     * @Route("/", name="lesson_index", methods={"GET"})
     */
    public function index(LessonRepository $lessonRepository): Response
    {
        return $this->render('lesson/index.html.twig', [
            'lessons' => $lessonRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="lesson_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $lesson = new Lesson();

        if ($request->getContent()) {
            $date = explode(" - ", $request->get('dateLesson'));
            $lesson->setStartDate(new \DateTime($date[0]));
            $lesson->setEndDate(new \DateTime($date[1]));
            $lesson->setName($request->get('name'));
            $lesson->setUser(
                $this->getDoctrine()
                    ->getRepository(User::class)
                    ->find((int)$request->get('formateur'))
            );
            $lesson->setClassroom(
                $this->getDoctrine()
                    ->getRepository(Classroom::class)
                    ->find((int)$request->get('classroom'))
            );
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lesson);
            $entityManager->flush();

            return $this->redirectToRoute('lesson_index');
        }
        
        return $this->render(
            'lesson/new.html.twig', [
            'formateurs' => $this->getDoctrine()
                ->getRepository(User::class)
                ->findBy(['status' => 'formateur']),
            'classrooms' => $this->getDoctrine()
                ->getRepository(Classroom::class)
                ->findAll()
            ]
        );
    }

    /**
     * @Route("/{id}", name="lesson_show", methods={"GET"})
     * @param Lesson $lesson
     * @return Response
     */
    public function show(Lesson $lesson): Response
    {
        return $this->render('lesson/show.html.twig', [
            'lesson' => $lesson,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="lesson_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Lesson $lesson): Response
    {
        if ($request->getContent()) {
            $date = explode(" - ", $request->get('dateLesson'));
            $lesson->setStartDate(new \DateTime($date[0]));
            $lesson->setEndDate(new \DateTime($date[1]));
            $lesson->setName($request->get('name'));
            $lesson->setUser(
                $this->getDoctrine()
                    ->getRepository(User::class)
                    ->find((int)$request->get('formateur'))
            );
            $lesson->setClassroom(
                $this->getDoctrine()
                    ->getRepository(Classroom::class)
                    ->find((int)$request->get('classroom'))
            );
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lesson);
            $entityManager->flush();
            return $this->redirectToRoute('lesson_index');
        }

        return $this->render(
            'lesson/edit.html.twig', [
            'lesson' => $lesson,
            'formateurs' => $this->getDoctrine()
                ->getRepository(User::class)
                ->findBy(['status' => 'formateur']),
            'classrooms' => $this->getDoctrine()
                ->getRepository(Classroom::class)
                ->findAll()
            ],
        );
    }

    /**
     * @Route("/{id}", name="lesson_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lesson $lesson): Response
    {
        if ($this->isCsrfTokenValid('delete'.$lesson->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lesson);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lesson_index');
    }
}
